﻿using UnityEngine;
using System.Collections;

public class PauseMenu_Script : MonoBehaviour {

    private bool m_paused;
    public GameObject pause_menu;
    
    public void MenuOn()
    {
        m_paused = true;
        pause_menu.SetActive(true);
    }	

    public void MenuOff()
    {
        m_paused = false;
        pause_menu.SetActive(false);
    }

    public void OnMenustatusChanged()
    {
        if (!m_paused)
        {
            MenuOn();
        }
        else if (m_paused)
        {
            MenuOff();
        }
    }

}
